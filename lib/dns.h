/* This file is part of Mailfromd.
   Copyright (C) 2005-2024 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef __mfd_dns_h
#define __mfd_dns_h

#include <mflib/status.h>

#define NELEMS(a) (sizeof(a) / sizeof((a)[0]))

/* NOTICE: When updating this enum, be sure to update dns_to_mf_status in
   dns.c accordingly. */
typedef enum {
	dns_success,
	dns_not_found,
	dns_failure,
	dns_temp_failure,
	dns_too_many
} dns_status;

typedef enum {
	dns_reply_ip,
	dns_reply_ip6,
	dns_reply_str
} dns_reply_type;

struct dns_reply {
	dns_reply_type type;
	int count;
	size_t maxcount;
	union {
		char **str;
		struct in_addr *ip;
		struct in6_addr *ip6;
		void *ptr;
	} data;
};

/*
 * Resolve modes for ns and mx lookups.  The DFL mode (use connection
 * type) is handled by MFL built-ins.
 */
enum {
	resolve_none = _MFL_RESOLVE_NONE,  /* Don't resolve hostnames. */
	resolve_ip4 = _MFL_RESOLVE_IP4,    /* Look up A records. */
	resolve_ip6 = _MFL_RESOLVE_IP6     /* Look up AAAA records. */
};

#define IPV4_INADDR_DOMAIN "in-addr.arpa"

/*
 * Max. buffer capacity for reversed dotted-quad IPv4: four three-digit
 * segments, each terminated by a dot, plus 1 byte for terminating \0.
 */
#define IPV4_DOTTED_BUFSIZE (4*4 + 1)

/*
 * Max. buffer capacity for formatting an in-addr.arpa address.
 */
#define	IPV4_INADDR_BUFSIZE \
	(IPV4_DOTTED_BUFSIZE + sizeof(IPV4_INADDR_DOMAIN) - 1)

#define IPV6_INADDR_DOMAIN "ip6.arpa"

/*
 * Max. buffer capacity for IPv6 addresses: 32 hexdigit
 * segments, each terminated by a dot, plus 1 byte for terminating \0.
 */
#define IPV6_DOTTED_BUFSIZE (32*2 + 1)

/*
 * Max. buffer capacity for formatting an ip6.arpa address.
 */
#define IPV6_INADDR_BUFSIZE \
	(IPV6_DOTTED_BUFSIZE + sizeof(IPV6_INADDR_DOMAIN) - 1)

#define IPMAX_DOTTED_BUFSIZE IPV6_DOTTED_BUFSIZE
#define IPMAX_INADDR_BUFSIZE IPV6_INADDR_BUFSIZE

void dnsbase_real_init(char *configtext);
void dnsbase_file_init(char const *file);

void dns_reply_init(struct dns_reply *reply, dns_reply_type type, size_t count);
void dns_reply_free(struct dns_reply *r);
size_t dns_reply_elsize(struct dns_reply *reply);

int dns_str_is_ipv4(const char *addr);
int dns_str_is_ipv6(const char *addr);

int dns_reverse_ipstr(const char *ipstr, char *revipstr, size_t len);
dns_status dns_reverse_name(const char *ipstr, char *buf, size_t bufsize);

dns_status dns_resolve_ipstr(const char *ipstr, struct dns_reply *reply);

dns_status dns_resolve_hostname(const char *host, int resolve_family, char **ipbuf);

dns_status soa_check(const char *name, int ip, struct dns_reply *repl);

dns_status a_lookup(const char *host, struct dns_reply *repl);
dns_status aaaa_lookup(const char *host, struct dns_reply *repl);

dns_status ptr_lookup(const char *host, struct dns_reply *reply);
dns_status txt_lookup(const char *name, struct dns_reply *repl);

dns_status ptr_validate(const char *ipstr, struct dns_reply *repl);

dns_status spf_lookup(const char *domain, char **record);
dns_status dkim_lookup(const char *domain, const char *sel, char ***retval);

dns_status mx_lookup(const char *host, int resolve, struct dns_reply *repl);

dns_status ns_lookup(const char *host, int resolve, struct dns_reply *reply);

#endif
