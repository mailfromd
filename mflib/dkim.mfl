/* Auxiliary DKIM functions.
   Copyright (C) 2007-2024 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

module 'dkim'.

#pragma regex push +extended

func dkim_signature_tag(string sig, string tag)
  returns string
do
  if sig matches "(^|;)%tag=([^;]+)"
    return \2
  fi
  switch tag
  do
  case 'c':
    return 'simple/simple'
  case 'q':
    return 'dns/txt'
  case 'i':
    return '@' . dkim_signature_tag(sig, 'd')
  done
  return ""
done

func dkim_verified_signature_tag(string tag)
  returns string
do
  return dkim_signature_tag(dkim_verified_signature, tag)
done

#pragma regex pop
