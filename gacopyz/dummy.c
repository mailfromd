#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <sys/types.h>
#include <sys/time.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "mfapi.h"

int
dummy_idle (gacopyz_conn_t conn)
{
	printf("IDLING\n");
	return 0;
}

sfsistat
dummy_connect(SMFICTX *ctx, char *hostname, _SOCK_ADDR *hostaddr)
{
	printf("CONNECT FROM: %s, family: %d\n", hostname,
	       hostaddr ? hostaddr->sa.sa_family : -1);
	return SMFIS_CONTINUE;
}
	      
sfsistat
dummy_envfrom(SMFICTX *ctx, char **argv)
{
	return SMFIS_CONTINUE;
}

sfsistat
dummy_eom(SMFICTX *ctx)
{
	static char newbody[] = "Zawartosc wiadomosci zostala usunieta\n"
		"wylacznie ze zlej woli administratora systemu\n"
		"--\n"
		"Bwana Budu\n";
	smfi_addheader(ctx, "X-Been-Here", "Gacopyz");
	smfi_insheader(ctx, 1, "X-New-Header", "nowy");
	smfi_chgheader(ctx,  "X-Duplicate-Header", 1, NULL);
	smfi_addrcpt(ctx, "gray@gnu.org.ua");
#if 0
	smfi_delrcpt(ctx, "<gray@localhost>");
	smfi_delrcpt(ctx, "gray");
#endif
	smfi_replacebody(ctx, newbody, sizeof newbody);
	return SMFIS_CONTINUE;
}

struct smfiDesc smfilter =
{
	"DummyFilter",	
	SMFI_VERSION,	
	SMFIF_ADDHDRS|SMFIF_CHGHDRS|SMFIF_ADDRCPT|SMFIF_DELRCPT|
	SMFIF_CHGBODY|SMFIF_QUARANTINE,
	dummy_connect,	        /* connection info filter */
	NULL,	        /* SMTP HELO command filter */
	dummy_envfrom,	        /* envelope sender filter */
	NULL,	        /* envelope recipient filter */
	NULL,	        /* header filter */
	NULL,           /* end of header */
	NULL,	        /* body block filter */
	dummy_eom,	/* end of message */
	NULL,	/* message aborted */
	NULL,	/* connection cleanup */
	NULL,   /* unknown command handler */
	NULL,   /* data handler */
	NULL,   /* negotiate */
	
	NULL,   /* child start */
	NULL,   /* child finish */
	NULL /* idle callback */
};

char *level_name[] = {
	"DEBUG",
	"INFO",
	"WARN",
	"ERR",
	"FATAL"
};

static void
stderr_log_printer(int level, char *fmt, va_list ap)
{
	fprintf(stderr, "%s: ", level_name[level]);
	vfprintf(stderr, fmt, ap);
	fprintf(stderr, "\n");
}

int
main(int argc, char **argv)
{
	if (!argv[1]) {
		fprintf(stderr, "usage: %s portspec\n", argv[0]);
		exit(1);
	}

	gacopyz_set_logger(stderr_log_printer);
	smfi_setdbg(1);

	if (smfi_setconn(argv[1]) == MI_FAILURE) {
		fprintf(stderr, "smfi_setconn: %s\n", strerror(errno));
		exit(1);
	}

	if (smfi_register(smfilter) == MI_FAILURE) {
		fprintf(stderr, "smfi_register failed\n");
		exit(1);
	}
	smfi_opensocket(1);
	return smfi_main();
}
