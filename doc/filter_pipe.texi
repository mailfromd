@c Copyright (C) 2005--2024 Sergey Poznyakoff
@c Permission is granted to copy, distribute and/or modify this document
@c under the terms of the GNU Free Documentation License, Version 1.3 or
@c any later version published by the Free Software Foundation; with no
@c Invariant Sections, with the Front and Back-Cover texts at your option. 
@cindex pipe
@cindex filter pipe
A @dfn{filter pipe} is a string consisting of filter invocations
delimited by pipe characters (@samp{|}).  Each invocation 
is a filter name optionally followed by a comma-separated list of
parameters.  Most filters can operate in two modes: @dfn{encode} and
@dfn{decode}.  Unless specified otherwise, filters are invoked in
encode mode.  To change the mode, the @code{encode} and @code{decode}
meta-filters are provided.  Argments to these filters are filter pipes
that will be executed in the corresponding mode.

The following Mailutils filters are available:

@deffn {Filter} 7bit
In encode mode, converts its input into 7-bit ASCII, by clearing the
8th bit on each processed byte. 

In decode mode, it operates exactly as the 8bit filter, i.e. copies
its input to the output verbatim.

The filter takes no arguments.
@end deffn

@deffn {Filter} 8bit
@deffnx {Filter} binary
Copies its input to output verbatim.
@end deffn

@deffn {Filter} base64
@deffnx {Filter} B
Encodes or decodes the input using the @code{base64} encoding.

The only difference between @code{BASE64} and @code{B} is that, in
encode mode, the former limits each ouput line length to 76 octets,
whereas the latter produces a contiguous stream of base64 data.

In decode mode, both filters operate exactly the same way.
@end deffn

@deffn {Filter} charset (@var{cset})
@deffnx {Filter} charset (@var{cset}, @var{fallback})
A convenience interface to the @code{iconv} filter, available for use
only in the @code{message_body_to_stream} function.  It decodes the
part of a MIME message from its original character set, which is
determined from the value of the @code{Content-Type} header, to the
destination character set @var{cset}.  Optional @var{fallback}
parameter specifies the representation fallback to be used for octets
that cannot be converted between the charater sets.  Its use is
described in @xref{iconv}.

This filter is normally takes its input from the @code{mimedecode}
filter, as in:

@example
message_body_to_stream(fd, msg, 'mimedecode|charset(utf-8)')
@end example

@xref{mimedecode}, for a detailed discussion.
@end deffn

@deffn {Filter} crlf
@deffnx {Filter} rfc822
Converts line separators from LF (ASCII 10) to CRLF (ASCII 13 10) and
vice-versa.

In decode mode, translates each CRLF to LF.  Takes no arguments.

In encode mode, translates each LF to CRLF.  If an optional argument
@samp{-n} is given, produces a @dfn{normalized} output, by preserving
each input CRLF sequence untouched (otherwise such sequences will be
are translated to CR CR LF).
@end deffn

@deffn {Filter} crlfdot
In encode mode, replaces each LF (@samp{\n} or ASCII 10) character with
CRLF (@samp{\r\n}, ASCII 13 10), and @dfn{byte-stuffs} the output by
producing an additional @samp{.} in front of any @samp{.} appearing at 
the beginning of a line in input.  Upon end of input, it outputs
additional @samp{.\r\n}, if the last output character was @samp{\n}, or
@samp{\r\n.\r\n} otherwise.

If supplied the @samp{-n} argument, it preserves each CRLF input
sequence untranslated (see the @code{CRLF} above).

In decode mode, the reverse is performed: each CRLF is replaced with a
single LF byte, and additional dots are removed from beginning of
lines.  A single dot on a line by itself marks the end of the stream and
causes the filter to return EOF.
@end deffn

@deffn {Filter} dot
In encode mode, @dfn{byte-stuffs} the input by outputting an
additional dot (@samp{.}) in front of any dot appearing at the
beginning of a line.  Upon encountering end of input, it outputs
additional @samp{.\n}.

In decode mode, the reverse is performed: additional dots are
removed from beginning of lines.  A single dot on a line by itself
(i.e. the sequence @samp{\n.\n}) marks the end of the stream and
causes the filter to return EOF.

This filter doesn't take arguments.
@end deffn

@deffn {Filter} from
Performs a traditional UNIX processing of lines starting with a
@samp{From} followed by a space character.

In encode mode, each @samp{From } at the beginning of a line is
replaced by @samp{>From }.

In decode mode, the reverse operation is performed: initial
greater-then sign (@samp{>}) is removed from any line starting with 
@samp{>From }.

The filter takes no arguments.
@end deffn

@deffn {Filter} fromrd
MBOXRD-compatible processing of envelope lines.

In encode mode, each @samp{From } optionally preceded by any number of
contiguous @samp{>} characters and appearing at the beginning of a
line is prefixed by another @samp{>} character on output.

In decode mode, the reverse operation is performed: initial
greater-then sign (@samp{>}) is removed from any line starting with
one or more @samp{>} characters followed by @samp{From }.
@end deffn

@deffn {Filter} header
This filter treats its input as a RFC-2822 email message.  It extracts
its header part (i.e. everything up to the first empty line) and
copies it to the output.  The body of the message is ignored.

The filter operates only in decode mode and takes no arguments.
@end deffn

@anchor{iconv}
@deffn {Filter} iconv (@var{src}, @var{dst} [, @var{fallback}])
Converts input from character set @var{src} to @var{dst}.  The filter
works the same way in both decode and encode modes.

@anchor{iconv fallback}
It takes two mandatory arguments: the names of the input (@var{src})
and output (@var{dst}) charset.  Optional third argument specifies
what to do when an illegal character sequence is encountered in the
input stream.  Its possible values are:

@table @option
@kwindex none
@item none
Raise a @code{e_ilseq} exception.

@kwindex copy-pass
@item copy-pass
Copy the offending octet to the output verbatim and continue
conversion from the next octet.

@kwindex copy-octal
@item copy-octal
Print the offending octet to the output using the C octal conversion
and continue conversion from the next octet.
@end table

The default is @code{copy-octal}.

The following example creates a @code{iconv} filter for converting from
@code{iso-8859-2} to @code{utf-8}, raising the @code{e_ilseq}
exception on the first conversion error:

@example
iconv(iso-8859-2, utf-8, none)
@end example
@end deffn

@deffn {Filter} inline-comment
@deffnx {Filter} inline-comment (@var{str}, [options])
In decode mode, the filter removes from the input all lines beginning
with a given @dfn{inline comment sequence} @var{str}.  The default comment
sequence is @samp{;} (a semicolon).

The following options modify the default behavior:

@table @option
@item -i, @var{str}
Emit line number information after each contiguous sequence of removed
lines.  The argument @var{str} supplies an @dfn{information starter}
-- a sequence of characters which is output before the actual line number.

@item -r
Remove empty lines, i.e. the lines that contain only whitespace characters.

@item -s
Squeeze whitespace.  Each sequence of two or more whitespace
characters encountered on input is replaced by a single space
character on output.

@item -S
A @dfn{whitespace-must-follow} mode.  A comment sequence is recognized
only if followed by a whitespace character.  The character itself is
retained on output.
@end table

In encode mode the @code{inline-comment} filter adds a comment-starter
sequence at the beginning of each line.  The default comment-starter
is @samp{;} and can be changed by specifying the desired comment
starter as the first argument.

The only option supported in this mode is @option{-S}, which enables the
whitespace-must-follow mode, in which a single space character (ASCII
20) is output after each comment sequence.
@end deffn
      
@deffn {Filter} linecon
@deffnx {Filter} linecon (-i, @var{str})
Implements a familiar UNIX line-continuation facility.  The filter
removes from itsinput stream any newline character immediately
preceded by a backslash.  This filter operates only in decode mode.

If given the arguments (@samp{-i}, @var{str}), enables the @dfn{line
number information facility}.  This facility emits current input line
number (prefixed with @var{str}) after each contiguous sequence of one
or more removed newline characters.  It is useful for implementing
parsers which are normally supposed to identify eventual erroneous
lines with their input line numbers.
@end deffn

@deffn {Filter} linelen (@var{n})
Limits the length of each output line to a certain number of octets.
It operates in encode mode only and requires a single parameter: the
desired output length in octets.  This filter makes no attempt to
analyze the lexical structure of the input: the newline caracters are
inserted when the length of the output line reaches a predefined
maximum.  Any newline characters present in the input are taken into
account when computing the input line length.
@end deffn

@deffn {Filter} mimedecode
This is a domain-specific filter available for use only with the
@code{message_body_to_stream} function.  It decodes the part of
a MIME message from whatever encoding that was used to store it
in the message to a stream of bytes.  @xref{mimedecode}.
@end deffn

@deffn {Filter} quoted-printable
@deffnx {Filter} Q
Encodes or decodes the input using the @dfn{quoted-printable}
encoding.
@end deffn

@deffn {Filter} XML
In encode mode, the @code{xml} filter converts input stream (which must
contain valid UTF-8 characters) into a form suitable for inclusion into
a XML or HTML document, i.e. it replaces @samp{<}, @samp{>}, and
@samp{&} with @samp{&lt;}, @samp{&gt;}, and @samp{&amp;},
correspondingly, and replaces invalid characters with their numeric
character reference representation.

In decode mode, a reverse operation is performed.

The filter does not take arguments.
@end deffn

      

   


      
      
   
   
