@c This file is part of the Mailfromd manual.
@c Copyright (C) 2008--2024 Sergey Poznyakoff
@c See file mailfromd.texi for copying conditions.
@macro xopindex{option,program,text}
@opindex \option\, --\option\ @r{\program\ option, \text\}
@end macro

@macro opsummary{option,program}
@ifclear ANCHOR--\option\
@set ANCHOR--\option\ 1
@anchor{--\option\}
@end ifclear
@xopindex{\option\, \program\, summary}
@end macro

@macro xprindex{option}
@ifclear ANCHOR-PR-\option\
@set ANCHOR-PR-\option\ 1
@anchor{pragma \option\}
@end ifclear
@end macro

@macro example-output{text}
@example
\text\
@end example
@end macro

@macro mtasimopt{option,text}
@mtindex \option\, --\option\, @r{@command{mtasim} option, \text\}
@end macro

@macro histref{version,ref,text}
@uref{http://mailfromd.man.gnu.org.ua/historic/\version\/html_node/\ref\.html,\text\}
@end macro


