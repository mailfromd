TEMPLATE /* */ -*- c -*-
/* This file is part of Mailfromd.
   Copyright (C) 2006-2024 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include "mailfromd.h"
#include "prog.h"
#include "optab.h"

%{extern void instr_%name(eval_environ_t); %}
%[DUMP][x]{extern void %x(prog_counter_t); %}
	  
struct optab optab[] = {
	{ "STOP", NULL, NULL, 0 },
	%{	{ "%NAME", instr_%name, %DUMP, %NWORDS }, %}
	{ NULL },
};

void
scan_code(prog_counter_t start, prog_counter_t end,
	  void (*fun)(prog_counter_t, struct optab *, void *), void *data)
{
	prog_counter_t i;

	for (i = start; i < end; i++) {
		if (mf_cell_c_value(prog[i], uint)) {
			struct optab *op;
			unsigned opcode = mf_cell_c_value(prog[i], uint);
			if (opcode > max_instr_opcode) {
				mu_error(_("%08lx: unknown opcode %u; aborting dump"),
					 (unsigned long) i, opcode);
				exit(1);
			}
			op = &optab[opcode];
			fun(i, op, data);
			i += op->length;
		} else 
			fun(i, &optab[opcode_nil], data);
	}
}

