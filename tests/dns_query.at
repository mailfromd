# This file is part of Mailfromd testsuite. -*- Autotest -*-
# Copyright (C) 2007-2024 Sergey Poznyakoff
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

m4_pushdef([DNS_QUERY_TEST],[
AT_CHECK([
MFT_REQUIRE_DNS
cleardb

AT_DATA([prog],
[require dns
require status
func main(...) returns number
do
  set n dns_query($1)
  echo dns_reply_count(n)
  loop for set i 0,
       while i < dns_reply_count(n),
       set i i + 1
  do
    echo dns_reply_string(n, i)
  done
  return 0
done  
])
mailfromd MAILFROMD_LOGOPTS MAILFROMD_OPTIONS --run prog 
],
[0],
[],
[$2])])

# ################################

AT_SETUP([dns_query: a])
AT_KEYWORDS([ip4 dns dns_ip4 dns_query dsn_query_ip4 dns_query_a])
DNS_QUERY_TEST([DNS_TYPE_A, "bkmx.test1.mf.gnu.org.ua", 1],
[3
192.0.2.3
192.0.2.4
192.0.2.5
])
AT_CLEANUP

# ################################

AT_SETUP([dns_query: ptr])
AT_KEYWORDS([ip4 dns dns_ip4 dns_query dns_query_ip4 dns_query_ptr])
DNS_QUERY_TEST([DNS_TYPE_PTR, "198.51.100.2"],
[1
mail1.test2.mf.gnu.org.ua
])
AT_CLEANUP

# ################################

AT_SETUP([dns_query: txt])
AT_KEYWORDS([ip4 dns dns_ip4 dns_query dns_query_ip4 dns_query_txt])
DNS_QUERY_TEST([DNS_TYPE_TXT, "test1.mf.gnu.org.ua", 1],
[2
Mailfromd test domain 1
v=spf1 -all
])
AT_CLEANUP

# ################################

AT_SETUP([dns_query: ns])
AT_KEYWORDS([ip4 dns dns_ip4 dns_query dns_query_ip4 dns_query_ns])
DNS_QUERY_TEST([DNS_TYPE_NS, "nstest.mf.gnu.org.ua", 1],
[3
ns.test2.mf.gnu.org.ua
ns1.test1.mf.gnu.org.ua
ns2.test1.mf.gnu.org.ua
])

DNS_QUERY_TEST([DNS_TYPE_NS, "nstest.mf.gnu.org.ua", 1, RESOLVE_IP4],
[3
192.0.2.6
192.0.2.7
198.51.100.5
])
AT_CLEANUP

# ################################

AT_SETUP([dns_query: aaaa])
AT_KEYWORDS([ip6 dns dns_ip6 dns_query dns_query_ip6 dns_query_aaaa])

MFT_WITH_PREREQ([MFT_REQUIRE_IPV6],[
DNS_QUERY_TEST([DNS_TYPE_NS, "nstest.mf.gnu.org.ua", 1, RESOLVE_IP6],
[3
2001:db8::6
2001:db8::7
2001:db8::1:0:0:5
])
])
AT_CLEANUP

# ################################

AT_SETUP([dns_query: mx])
AT_KEYWORDS([ip4 dns dns_ip4 dns_query dns_query_ip4 dns_query_mx dns_query_mx_ip6])

DNS_QUERY_TEST([DNS_TYPE_MX, "test1.mf.gnu.org.ua"],
[3
mail.test1.mf.gnu.org.ua
mail.test2.mf.gnu.org.ua
bkmx.test1.mf.gnu.org.ua
])

DNS_QUERY_TEST([DNS_TYPE_MX, "test1.mf.gnu.org.ua", 1],
[3
bkmx.test1.mf.gnu.org.ua
mail.test1.mf.gnu.org.ua
mail.test2.mf.gnu.org.ua
])

DNS_QUERY_TEST([DNS_TYPE_MX, "test1.mf.gnu.org.ua", 1, RESOLVE_IP4],
[7
192.0.2.2
192.0.2.3
192.0.2.4
192.0.2.5
198.51.100.2
198.51.100.3
198.51.100.4
])
AT_CLEANUP

AT_SETUP([dns_query: mx (IPv6)])
AT_KEYWORDS([ip6 dns dns_ip6 dns_query dns_query_ip6 dns_query_mx_ip6])

MFT_WITH_PREREQ([MFT_REQUIRE_IPV6],[
DNS_QUERY_TEST([DNS_TYPE_MX, "test1.mf.gnu.org.ua", 1, RESOLVE_IP6],
[3
2001:db8::2
2001:db8::3
2001:db8::4
])
])

AT_CLEANUP

m4_popdef([DNS_QUERY_TEST])
