# This file is part of Mailfromd testsuite. -*- Autotest -*-
# Copyright (C) 2009-2024 Sergey Poznyakoff
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

AT_SETUP([Try-catch: returning from try])
AT_KEYWORDS([try catch return try-catch trycatch try-catch01 trycatch01])

# Description: Check returning from the try branch of a `try-catch'
# construct.

MFT_RUN([
func throwcheck()
  returns number
do
  try
  do
    return 0
  done
  catch *
  do
    echo "Caught exception: $1, $2"
    return 1
  done

  return 2
done

func main(...)
  returns number
do
  echo throwcheck()
done
],
[],
[0],
[],
[0
])

AT_CLEANUP
