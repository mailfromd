# This file is part of Mailfromd testsuite. -*- Autotest -*-
# Copyright (C) 2009-2024 Sergey Poznyakoff
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

AT_SETUP([module: require])
AT_KEYWORDS([module module01 require variable variables])

AT_DATA([mod1.mfl],
[[module "mod1".
require mod2

func mod1_print()
do
  echo "%__module__:%__line__: %__function__ called"
done

func mod2_print()
do
  mod_print2()
done
]])

AT_DATA([mod2.mfl],
[[module "mod2".

func mod_print2()
do
  echo "%__module__:%__line__: %__function__ called"
done
]])

MFT_RUN(
[require 'mod2'
func main(...)
  returns number
do
  mod_print2()
  return 0
done
],
[],
[0],
[],
[mod2:5: mod_print2 called
])

AT_CLEANUP

  