# This file is part of Mailfromd testsuite. -*- Autotest -*-
# Copyright (C) 2007-2024 Sergey Poznyakoff
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# We need a recent Autotest.
m4_version_prereq([2.52g])

dnl # Standard exit codes (from sysexits.h)
m4_define([EX_OK],          0)       dnl successful termination 
m4_define([EX__BASE],       64)      dnl base value for error messages 
m4_define([EX_USAGE],       64)      dnl command line usage error 
m4_define([EX_DATAERR],     65)      dnl data format error 
m4_define([EX_NOINPUT],     66)      dnl cannot open input 
m4_define([EX_NOUSER],      67)      dnl addressee unknown 
m4_define([EX_NOHOST],      68)      dnl host name unknown 
m4_define([EX_UNAVAILABLE], 69)      dnl service unavailable 
m4_define([EX_SOFTWARE],    70)      dnl internal software error 
m4_define([EX_OSERR],       71)      dnl system error (e.g., can't fork) 
m4_define([EX_OSFILE],      72)      dnl critical OS file missing 
m4_define([EX_CANTCREAT],   73)      dnl can't create (user) output file 
m4_define([EX_IOERR],       74)      dnl input/output error 
m4_define([EX_TEMPFAIL],    75)      dnl temp failure; user is invited to retry 
m4_define([EX_PROTOCOL],    76)      dnl remote error in protocol 
m4_define([EX_NOPERM],      77)      dnl permission denied 
m4_define([EX_CONFIG],      78)      dnl configuration error 

m4_pushdef([MAILFROMD_OPTIONS],[-P$TESTDIR $MFOPTS $MFADDOPTS])
m4_define([MAILFROMD_LOGOPTS],[--stderr])
m4_define([MFT_SKIP_TEST],[exit 77])

dnl MFT_UNPRIVILEGED_PREREQ - Skip test if running at root privileges
m4_define([MFT_UNPRIVILEGED_PREREQ],[
# Skip test if running at root privileges
echo "test" > $[]$
chmod 0 $[]$
cat $[]$ > /dev/null 2>&1
result=$?
rm -f $[]$
test $result -eq 0 && MFT_SKIP_TEST
])

m4_define([MFT_REQUIRE_DNS],[at_resolv_conf || MFT_SKIP_TEST])
m4_define([MFT_REQUIRE_SMTP],
[MFT_REQUIRE_DNS
portprobe $1 25 || MFT_SKIP_TEST
])
m4_define([MFT_REQUIRE_IPV6],[ipv6on || MFT_SKIP_TEST])
m4_pushdef([__MFT_TEST_PREREQ])
 
m4_define([MFT_WITH_PREREQ],[
m4_pushdef([__MFT_TEST_PREREQ],[$1])
$2
m4_popdef([__MFT_TEST_PREREQ])])

dnl MFT_TEST(TEXT,[ARGS],
dnl          [STATUS], [STDOUT], [STDERR], [RUN-IF-FAIL], [RUN-IF-PASS])
m4_define([MFT_TEST],[
dnl Save the program
AT_DATA([prog],[$1
])
AT_CHECK([__MFT_TEST_PREREQ
TESTDIR=$(pwd)
mailfromd MAILFROMD_LOGOPTS MAILFROMD_OPTIONS --test prog $2],m4_shift(m4_shift($@)))
])

dnl MFT_RUN(TEXT,[ARGS],
dnl         [STATUS], [STDOUT], [STDERR], [RUN-IF-FAIL], [RUN-IF-PASS])
m4_define([MFT_RUN],[
dnl Save the program
AT_DATA([prog],[$1
])
AT_CHECK([__MFT_TEST_PREREQ
TESTDIR=$(pwd)
mailfromd MAILFROMD_LOGOPTS MAILFROMD_OPTIONS --run prog $2],
m4_shift2($@))
])

m4_define([mailfromd_script_warning],[mailfromd: warning: script file is given without full file name
])

m4_pushdef([mailfromd_debug_header],[dnl
mailfromd: warning: program started without full file name
mailfromd: warning: restart (SIGHUP) will not work
mailfromd: mailfromd started
])
m4_pushdef([mailfromd_debug_footer],[mailfromd: mailfromd terminating
])

m4_pushdef([MTASIM_PROLOGUE])

dnl MFT_MTASIM(FILTER, SCRIPT, 
dnl            [STATUS], [STDOUT], [STDERR], [RUN-IF-FAIL], [RUN-IF-PASS])
m4_define([MFT_MTASIM],[
 AT_DATA([filter],[$1])
 AT_DATA([script],[$2])
 m4_pushdef([mailfromd_debug_header],mailfromd_script_warning[]mailfromd_debug_header)
 AT_CHECK(
  [MFT_UNPRIVILEGED_PREREQ
   TESTDIR=$(pwd)
   __MFT_TEST_PREREQ
   MTASIM_PROLOGUE
   X_OPT=auto
   if test -n "$STATEDIR"; then
     if test -d $STATEDIR; then
       X_OPT=dir:$STATEDIR
     fi
   fi
   mtasim -DTESTDIR="$TESTDIR" --stdio -X$X_OPT --statedir -- MAILFROMD_LOGOPTS MAILFROMD_OPTIONS filter < script 2>err >out
   rc=$?
   sed -e 's/^mailfromd: info: *//' err >&2
   exit $rc],
   [$3],
   [$4],
   [m4_if([$5],[ignore],[ignore],[mailfromd_debug_header[]$5[]mailfromd_debug_footer])],
   m4_shift(m4_shift(m4_shift(m4_shift(m4_shift($@))))))
 m4_popdef([mailfromd_debug_header])])

m4_define([MFT_WITH_MAILFROMD_OPTIONS],[
m4_pushdef([MAILFROMD_OPTIONS],[$MFOPTS $MFADDOPTS ]$1)
$2
m4_popdef([MAILFROMD_OPTIONS])])

AT_INIT

AT_TESTED([mailfromd])

AT_BANNER([Version])
m4_include([version.at])
m4_include([vercmp.at])
m4_include([vercmp01.at])
m4_include([vercmp02.at])

AT_BANNER([Scope of Visibility])
m4_include([static01.at])
m4_include([static02.at])
m4_include([public.at])

AT_BANNER([Variable Shadowing])
m4_include([shadow.at])
m4_include([ashadow.at])

AT_BANNER([MFL])
m4_include([enum.at])
m4_include([ack.at])

m4_include([arg.at])
m4_include([vararg.at])

m4_include([alias.at])

m4_include([bctx00.at])
m4_include([bctx01.at])

m4_include([fctx00.at])
m4_include([fctx01.at])

m4_include([ml.at])
m4_include([ml01.at])

m4_include([declvar.at])
m4_include([arginit.at])
m4_include([vapass.at])
m4_include([stkadj.at])
m4_include([shiftadj.at])
m4_include([ignoreret.at])

AT_BANNER([Conditionals])
m4_include([cond01.at])
m4_include([cond02.at])
m4_include([cond04.at])
m4_include([cond06.at])

AT_BANNER([Switch])
m4_include([switchn1.at])
m4_include([switchn2.at])
m4_include([switchn3.at])
m4_include([switchn4.at])

m4_include([switchs1.at])
m4_include([switchs2.at])
m4_include([switchs3.at])
m4_include([switchs4.at])

AT_BANNER([Breaking Loops])
m4_include([next01.at])
m4_include([next02.at])
m4_include([next03.at])

AT_BANNER([Macros])
m4_include([macros.at])
m4_include([expstr.at])
m4_include([miltermacros.at])

AT_BANNER([Exception handling])
m4_include([catch.at])
m4_include([catch01.at])
m4_include([catch02.at])
m4_include([catch03.at])

m4_include([trycatch01.at])
m4_include([trycatch02.at])
m4_include([trycatch03.at])
m4_include([trycatch04.at])
m4_include([trycatch05.at])
m4_include([trycatch06.at])
m4_include([trycatch07.at])
m4_include([trycatch08.at])
m4_include([trycatch09.at])
m4_include([trycatch10.at])
m4_include([trycatch11.at])
m4_include([trycatch12.at])
m4_include([trycatch13.at])
m4_include([trycatch14.at])
m4_include([trycatch15.at])
m4_include([trycatch16.at])

m4_include([strace.at])

AT_BANNER([Handlers])
m4_include([startup.at])
m4_include([progord.at])
m4_include([progdup.at])

AT_BANNER([Actions])
m4_include([accept.at])
m4_include([reject.at])
m4_include([tempfail.at])
m4_include([hook.at])

AT_BANNER([Handler Arguments])
m4_include([connargs.at])
m4_include([heloargs.at])
m4_include([fromargs.at])
m4_include([rcptargs.at])
m4_include([hdrargs.at])
m4_include([eohargs.at])
m4_include([bodyargs.at])
m4_include([eomargs.at])

AT_BANNER([Connect Handler])
m4_include([connect00.at])
m4_include([connect01.at])
m4_include([connect02.at])
m4_include([connect03.at])

AT_BANNER([Modules])
m4_include([module01.at])
m4_include([module02.at])
m4_include([module03.at])
m4_include([module04.at])
m4_include([module05.at])
m4_include([module06.at])
m4_include([module07.at])

AT_BANNER([mfmod])
m4_include([mfmod01.at])
m4_include([mfmod02.at])
m4_include([mfmod03.at])

AT_BANNER([Engine Features])
m4_include([numrcpt.at])
m4_include([rset.at])

AT_BANNER([DNS lookups])
m4_include([resolv_a.at])
m4_include([resolv_ptr.at])
m4_include([resolv_txt.at])
m4_include([resolv_mx.at])
m4_include([resolv_spf.at])
m4_include([resolv_ptr_val.at])
m4_include([resolv_cnamechain.at])

AT_BANNER([DNS Functions])
m4_include([dns_query.at])
m4_include([resolve.at])
m4_include([rescname.at])
m4_include([hostname.at])
m4_include([hasmx.at])
m4_include([ismx.at])

AT_BANNER([Relayed])
m4_include([relayed01.at])
m4_include([relayed02.at])

AT_BANNER([Greylisting])
m4_include([greylist.at])
m4_include([greylist-ct.at])

AT_BANNER([Current Message])
m4_include([curmsg.at])
m4_include([nulmsg.at])

AT_BANNER([Header Functions])
m4_include([hdr-count.at])
m4_include([hdr-get.at])
m4_include([hdr-getn.at])
m4_include([hdr-gete.at])
m4_include([hdr-itr.at])
m4_include([hdr-all.at])
m4_include([hdr-cap.at])
m4_include([hdr-mul.at])

AT_BANNER([Input/Output])
m4_include([read.at])
m4_include([write.at])
m4_include([getline.at])
m4_include([getdelim.at])
m4_include([tempfile.at])

AT_BANNER([SPF macro expansions])
m4_include([spfexp.at])

AT_BANNER([SPF])
m4_include([spf.at])

AT_BANNER([Individual Functions])
m4_include([strings.at])
m4_include([tr.at])
m4_include([dc.at])
m4_include([sq.at])
m4_include([sed.at])
m4_include([strftime.at])
m4_include([cidr.at])

AT_BANNER([Assorted Checks])
m4_include([regopt0.at])
m4_include([setvar.at])

m4_include([shellmagic.at])
m4_include([commaize.at])

AT_BANNER([Poll])
m4_include([poll.at])
m4_include([poll01.at])
m4_include([poll02.at])
m4_include([poll03.at])
m4_include([poll04.at])

AT_BANNER([Specific modules])

m4_include([cdb.at])
m4_include([dnsbl.at])
