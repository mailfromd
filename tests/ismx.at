# This file is part of Mailfromd testsuite. -*- Autotest -*-
# Copyright (C) 2007-2024 Sergey Poznyakoff
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

AT_SETUP([Ismx call])
AT_KEYWORDS([ip4 dns dns_ip4 ismx ismx_ip4])

MFT_WITH_PREREQ([MFT_REQUIRE_DNS],

[MFT_RUN(
[func main(...)
  returns number
do
  echo primitive_ismx($1, $2)
  return 0
done
],
[test1.$MF_TOPDOMAIN mail.test2.$MF_TOPDOMAIN],
[0],
[],
[1
])

])

AT_CLEANUP

# ###############

AT_SETUP([Ismx call (IPv6)])
AT_KEYWORDS([ip6 dns dns_ip6 ismx ismx_ip6])

MFT_WITH_PREREQ([MFT_REQUIRE_DNS
MFT_REQUIRE_IPV6],

[MFT_RUN(
[func main(...)
  returns number
do
  echo primitive_ismx($1, $2)
  return 0
done
],
[test2.$MF_TOPDOMAIN mail6.$MF_TOPDOMAIN],
[0],
[],
[1
])

])

AT_CLEANUP
