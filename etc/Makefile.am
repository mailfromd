# This file is part of Mailfromd.
# Copyright (C) 2005-2024 Sergey Poznyakoff
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

EXTRA_DIST = \
 mailfromd.mfl\
 rc.in\
 sendmail-8.13.7.connect.diff\
 sendmail-8.14.3.connect.diff\
 postfix-macros.sed

noinst_SCRIPTS = rc.mailfromd
pkgdata_DATA = postfix-macros.sed

DEFAULT_PIDFILE = $(DEFAULT_STATE_DIR)/mailfromd.pid
CLEANFILES = rc.mailfromd

.in.mailfromd:
	$(AM_V_GEN)\
        f=$(sbindir)/`echo "mailfromd" | sed 's,^.*/,,;$(transform);s/$$/$(EXEEXT)/'`; \
	sed "s,<DEFAULT_PIDFILE>,$(DEFAULT_PIDFILE),;s,<DEFAULT_DAEMON>,$$f," $< > $@ && chmod +x $@

MFL_LINT = $(MFL_LINT_$(V))
MFL_LINT_ = $(MFL_LINT_$(AM_DEFAULT_VERBOSITY))
MFL_LINT_0 = @echo LINT $<;

SUFFIXES = .mfl .lint

.mfl.lint:
	$(MFL_LINT)$(top_builddir)/src/mailfromd \
                --no-config \
                -I$(top_srcdir)/mflib \
                -P$(top_srcdir)/mflib \
                -P$(top_builddir)/mflib \
                --lint $<

check-am: mailfromd.lint

install-data-local:
	@test -z "$(DESTDIR)$(sysconfdir)" || $(mkdir_p) "$(DESTDIR)$(sysconfdir)"
	@if test -r $(DESTDIR)$(sysconfdir)/mailfromd.mfl; then :; \
	else ${INSTALL} -m 644 $(top_srcdir)/etc/mailfromd.mfl \
               $(DESTDIR)$(sysconfdir)/mailfromd.mfl; fi
