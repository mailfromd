/* This is a sample filter for Mailfromd.       -*- mfl -*-
   Site administrators are urged to write nicer versions.
   Copyright (C) 2005-2024 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#pragma regex +extended +icase

require status
require dns
require localdomain
require rateok
require poll
require callout

# Set email address used in callout MAIL command. Default is <>.
#set mailfrom_address "<>"
# If mailfromd does not determine your local domain correctly, uncomment this,
# replace DOMAIN with your local domain, and comment out the assignment to
# this variable in the 'begin' block below.
#set ehlo_domain "DOMAIN"
  
number gltime interval("1 hour")

number need_greylist 0

const whitelist_db "/etc/mail/whitelist.db"

func cachestr()
  returns string
do
  if cache_used
    return "[CACHED] "
  else 
    return ""
  fi
done

prog envfrom
do
  if $f == ""
    pass
  elif relayed(hostname($client_addr))
    pass
  elif access(whitelist_db, R_OK) and dbmap(whitelist_db, $client_addr)
    accept
  elif hostname($client_addr) matches ".*(adsl|sdsl|hdsl|ldsl|dialin|dialup|ppp|dhcp|dynamic).*"
    reject 550 5.7.1 "Use your SMTP relay"
  elif hostname($client_addr) matches ".*-[0-9]{1,3}-[0-9]{1,3}-[0-9]{1,3}-[0-9]{1,3}.*"
    set need_greylist 1
  elif $f mx fnmatches "*.yahoo.com"
    or $f mx fnmatches "*.namaeserver.com"
    set need_greylist 1
  else
    on poll $f do
    when success:
      pass
    when not_found or failure:
      reject 550 5.1.0 cachestr() . "Sender validity not confirmed"
    when temp_failure:
      tempfail
    done
  fi

  if not rateok("$f-$client_addr", interval("1 hour 30 minutes"), 100)
    tempfail 450 4.7.0 "Mail sending rate exceeded. Try again later"
  fi
done

prog envrcpt
do
  if need_greylist
    if greylist("${client_addr}-$f-${rcpt_addr}", gltime)
      if greylist_seconds_left == gltime
	tempfail 450 4.7.0
	         "You are greylisted for %gltime seconds"
      else
	tempfail 450 4.7.0
                "Still greylisted for %greylist_seconds_left seconds"
      fi
    fi
  fi
done


