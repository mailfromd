## This file is part of mailfromd                   -*- makefile-automake -*-
## Copyright (C) 2022-2024 Sergey Poznyakoff
##
## Mailfromd is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 3, or (at your option)
## any later version.
##
## Mailfromd is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with mailfromd.  If not, see <http://www.gnu.org/licenses/>.
##
## As a special exception, you may create a larger work that contains
## part or all of this file and distribute that work under terms of your
## choice.

# Makefile for <MODNAME>                        -*- makefile-automake -*-

# Define installation directory for mfmods. The substvar MFMODDIR is defined
# by the AC_MFMOD autoconf macro.
mfmoddir=@MFMODDIR@
# Define installation directory for MFL interface module files.
mfldir=@MFLDIR@
# Define variables needed to build the shared library.
mfmod_LTLIBRARIES = mfmod_<MODNAME>.la
mfmod_<MODNAME>_la_SOURCES = mfmod_<MODNAME>.c
AM_LDFLAGS = -module -export-dynamic -avoid-version -rpath '$(mfmoddir)'

# The <MODNAME>.mf file provides MFL interface to C functions.  By default
# it is installed alongside the mfmod_<MODNAME>.so module.  Perhaps a better
# idea is to install it somewhere in the include path of mailfromd.
mfl_DATA=<MODNAME>.mfl
EXTRA_DIST=$(mfl_DATA)

# Defining mfmoddir during distcheck ensures that it can be run by
# a non-privileged user, no matter how mailfromd was actually installed.
AM_DISTCHECK_CONFIGURE_FLAGS=\
  --with-mfmoddir='$$(pkglibdir)'\
  --with-mfldir='$$(pkglibdir)'
