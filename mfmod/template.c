/* A stub file for the <MODNAME> module.  Edit it and remove this comment. */
#include "config.h"
#include <mailfromd/mfmod.h>
#include <mailfromd/exceptions.h>
#include <mailutils/mailutils.h>

/*
 * version()
 * ---------
 * This function is for illustrative purpose only.  Remove it, if you
 * don't need it.
 *
 * Arguments: None
 * Returns: Version of <MODNAME> (string).
 */
int
version(long count, MFMOD_PARAM *param, MFMOD_PARAM *retval)
{
	ASSERT_ARGCOUNT(retval, count, 0);
	retval->type = mfmod_string;
	if ((retval->string = malloc(sizeof(VERSION))) == NULL)
		return -1;
	strcpy(retval->string, VERSION);
	return 0;
}

